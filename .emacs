(make-directory "~/.emacs.d/elpa/" t)
(let ((default-directory "~/.emacs.d/elpa"))
  (normal-top-level-add-subdirs-to-load-path))
(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
(mapc
 (lambda (package)
   (unless (package-installed-p package)
     (progn (message "installing %s" package)
	    (package-refresh-contents)
	    (package-install package))))
 '(auto-complete org web-mode php-mode python-mode))
(eval-and-compile
  (require 'cl nil 'noerror)
  (require 'pixilang-mode nil 'noerror)
  (require 'auto-complete nil 'noerror)
  (require 'emacs-sos nil 'noerror)
  (require 'hackernews nil 'noerror)
  (require 'transmission nil 'noerror)
  (require 'dired nil 'noerror)
  (require 'auto-complete-config nil 'noerror)
  (require 'org-install nil 'noerror)
  (require 'ido nil 'noerror)
  (require 'ibuffer nil 'noerror)
  (require 'web-mode nil 'noerror)
  (require 'linum nil 'noerror)
  (require 'portage nil 'noerror))


(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.pixi\\'" . pixilang-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
(add-to-list 'auto-mode-alist '("\\.twig\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

;;;;;; ; ; ;; ;  ; ; ;
;;;;;;;;; ;  ;; ; 
;;;;;; ;  ; ; ; ; ; 
;;; Func 
;;; ;; ;  ;;; ; ; 
;;;;; ;; ;; ;

(defun move-line-up ()
    (interactive)
    (transpose-lines 1)
    (forward-line -2)
    (indent-according-to-mode))

(defun move-line-down ()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(defun toggle-fullscreen ()
  (interactive)
  (set-frame-parameter nil 'fullscreen (if (frame-parameter nil 'fullscreen) nil 'fullboth)))

(defun google ()
  (interactive)
  (browse-url
   (concat
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q="
    (url-hexify-string (if mark-active
         (buffer-substring (region-beginning) (region-end))
       (read-string "Google: "))))))

(defun youtube ()
  (interactive)
  (browse-url
   (concat
    "http://www.youtube.com/results?search_query="
    (url-hexify-string (if mark-active
                           (buffer-substring (region-beginning) (region-end))
                         (read-string "Search YouTube: "))))))

(defun eshell-life-is-too-much ()
  "Kill the current buffer (or bury it).  Good-bye Eshell."
  (interactive)
  (if (not eshell-kill-on-exit)
      (bury-buffer)
    (kill-buffer (current-buffer))))

(defun prelude-open-with (arg)
  (interactive "P")
  (when buffer-file-name
    (shell-command (concat
                    (cond
                     ((and (not arg) (eq system-type 'darwin)) "open")
                     ((and (not arg) (member system-type '(gnu gnu/linux gnu/kfreebsd))) "xdg-open")
                     (t (read-shell-command "Open current file with: ")))
                    " "
                    (shell-quote-argument buffer-file-name)))))

(defun my-add-pretty-lambda ()
  (setq prettify-symbols-alist
	'(
	  ("lambda" . 955)
	  ("map" . 8614)
	  ("not" . ?¬)
	  (">=" . ?≥)
	  ("and" . ?∧)
	  ("or" . ?∨))))

(defun open-in-browser()
  (interactive)
  (let ((filename (buffer-file-name)))
    (browse-url (concat "file://" filename))))

(defun my-browse-url-seamonkey-new-tab (url &optional new-window)
      ;; new-window ignored                                                         
      "Open URL in a new tab in Seamonkey."
      (interactive (browse-url-interactive-arg "URL: "))
      (unless
          (string= ""
                   (shell-command-to-string
                    (concat "palemoon-bin -remote 'openURL(" url ",new-tab)'")))
        (message "Starting Palemoon...")
        (start-process (concat "palemoon-bin " url) nil "palemoon-bin" url)
        (message "Starting Palemoon...done")))

;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;
;;;;;;; Keys
;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;

(define-key global-map [f1] 'delete-other-windows)
(define-key global-map [f2] 'other-window)
(define-key global-map [XF86Reload] 'other-window)
(define-key global-map [f3] 'split-window-vertically)
(define-key global-map [f4] 'split-window-horizontally)
(global-set-key [f5] 'jabber-connect)
(global-set-key [XF86Mail] 'gnus)
(global-set-key (kbd "C-f") 'isearch-forward)
(global-set-key (kbd "C-S-f") 'isearch-backward)
(global-set-key (kbd "C-x C-d") (lambda () (interactive) (dired "~/Sources")))
(global-set-key [XF86Explorer] (lambda () (interactive) (speedbar)))
(global-set-key [(meta shift up)] 'move-line-up)
(global-set-key [(meta shift down)] 'move-line-down)
;;(global-set-key [XF86Search] 'google)
;;(global-set-key (kbd "C-c y") 'youtube)
(global-set-key [XF86Search] 'emacs-sos)
(global-set-key [XF86Calculator] 'transmission)
(global-set-key [XF86Favorites] 'hackernews)
(global-set-key (kbd "C-c E")  'erase-buffer)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c ix") 'ix)
(global-set-key (kbd "C-c p") 'portage-search)
(global-set-key [XF86HomePage] 'open-in-browser)
(define-key global-map (kbd "C-c M-3") (lambda () (interactive) (insert "£")))
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key "\M-x" (lambda () (interactive) (call-interactively (intern (ido-completing-read "M-x " (all-completions "" obarray 'commandp))))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode -1)
 '(browse-url-browser-function (quote browse-url-default-browser))
 '(custom-enabled-themes (quote (ample-zen)))
 '(custom-safe-themes
   (quote
    ("1db337246ebc9c083be0d728f8d20913a0f46edc0a00277746ba411c149d7fe5" default)))
 '(delete-selection-mode t)
 '(electric-indent-mode 1)
 '(electric-pair-mode 1)
 '(github-notifier-mode nil)
 '(github-notifier-token " a5ce998fdffc5f929e4bf8fe2efdd7c710ecc660")
 '(nyan-mode t)
 '(package-selected-packages
   (quote
    (jabber-otr portage-navi google-translate git github-browse-file github-issues ix slime wget websocket web-server web-mode web wc-mode vlf smartparens python-mode php-extras php-completion org-blog nssh jabber hackernews emacs-cl dired-du auto-indent-mode auto-highlight-symbol async)))
 '(scroll-bar-mode -1)
 '(send-mail-function (quote smtpmail-send-it))
 '(show-paren-mode t)
 '(tooltip-mode -1))

(defadvice linum-on (around linum-on-inhibit-for-modes)
  "Stop the load of linum-mode for some major modes."
    (unless (member major-mode linum-mode-inhibit-modes-list)
      ad-do-it))

(setq-default inferior-lisp-program "ccl")
(setq-default ac-auto-start t)
(setq-default ac-auto-show-menu t)
;;(setq-default lisp-indent-function 'common-lisp-indent-function)
;;(setq-default indent-tabs-mode nil)
;;(setq-default tab-width 4)
(setq frame-title-format "GNU Emacs: %b")
(setq dired-recursive-deletes 'top)
(setq whow-paren-style 'expression)
(setq inhibit-startup-screen -1)
(setq use-dialog-box nil)
(setq redisplay-dont-pause t)
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq auto-save-file-list nil)
(setq linum-format "%s ")
(setq linum-mode-inhibit-modes-list '(eshell-mode
                                      shell-mode
                                      erc-mode
                                      jabber-roster-mode
                                      jabber-chat-mode
                                      gnus-group-mode
                                      gnus-summary-mode
                                      gnus-article-mode))
(setq word-wrap t)
(setq browse-url-browser-function 'my-browse-url-seamonkey-new-tab)
(setq indent-line-function 'insert-tab)
(setq ido-virtual-buffers t)
(setq ido-enable-flex-matching t)
(setq scroll-step 1)
(setq scroll-margin 10)
(setq scroll-conservatively 10000)
(setq search-highlight t)
(setq query-replace-highlight t)
(setq ido-enable-flex-matching t)
(setq ido-default-file-method 'selected-window)
(setq ido-default-buffer-method 'selected-window)
(setq tramp-default-method "ssh")
(setq max-mini-window-height 0.5)
(setq gnus-select-method '(nntp "news.tin.org"))
(setq rcirc-server-alist '(("irc.freenode.net" :port 6697 :encryption tls :channels ("#gentoo-ru" "#gentoo-chat-ru"))))
(setq org-agenda-files (list "~/.org/work.org" "~/.org/timetracker.org" "~/.org/home.org"))
(setq org-log-done t)
(setq ibuffer-formats '((mark modified read-only " " (name 18 200 :left :elide) " " filename-and-process)))
(setq ibuffer-saved-filter-groups
      (quote (("default"
                   ("dired"       (mode . dired-mode))
                   ("perl"        (mode . perl-mode))
		   ("python"      (mode . python-mode))
		   ("php"         (mode . php-mode))
		   ("html"        (mode . web-mode))
		   ("common lisp" (mode . lisp-mode))
           ("planner"     (or
                           (name . "^\\*Calendar\\*$")
                           (name . "^\\*Org Agenda\\*$")
                           (name . "^diary$")
                           (mode . muse-mode)
                           (mode . org-mode)))
		   ("jabber"      (or
                           (mode . jabber-chat-mode)
                           (mode . jabber-roster-mode)))
           ("emacs"       (or
                           (name . "^\\*scratch\\*$")
                           (name . "^\\.emacs")
                           (name . "^\\*Messages\\*$")))
           ("gnus"        (or
                           (mode . message-mode)
                           (mode . bbdb-mode)
                           (mode . mail-mode)
                           (mode . gnus-group-mode)
                           (mode . gnus-summary-mode)
                           (mode . gnus-article-mode)
                           (name . "^\\.bbdb$")
                           (name . "^\\.newsrc-dribble")))))))

(setq jabber-account-list
 '(("ridouchire@jabber.ru/emacs"
   (:network-server . "jabber.ru")
    (:connection-type . ssl))))

(setq user-mail-address "<ridouchire@gmail.com>"
      user-full-name "Ritsuka Aoyagi")

(setq gnus-select-method
      '(nnimap "gmail"
	       (nnimap-address "imap.gmail.com")  ; it could also be imap.googlemail.com if that's your server.
	       (nnimap-server-port "imaps")
	       (nnimap-stream ssl)))

(setq smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")

;;; Auto-Complete Mode
(defvar *sources* (list
		   'lisp-mode
		   'ac-source-semantic
                   'ac-source-functions
                   'ac-source-variables
                   'ac-source-dictionary
                   'ac-source-words-in-all-buffer
                   'ac-source-files-in-current-dir))
(let (source)
  (dolist (source *sources*)
    (add-to-list 'ac-sources source)))
(add-to-list 'ac-modes 'lisp-mode)

(fset 'yes-or-no-p 'y-or-n-p)
(line-number-mode t)
(global-linum-mode 1)
(column-number-mode t)
(ad-activate 'linum-on)
(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(scroll-bar-mode -1)
(display-time-mode -1)
(size-indication-mode -1)
(global-visual-line-mode -1)
(put 'erase-buffer 'disabled nil)
(global-prettify-symbols-mode 1)
(ac-config-default)
(global-auto-complete-mode t)
(ido-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(add-hook 'ibuffer-mode-hook (lambda () (ibuffer-switch-to-saved-filter-groups "default")))

(require 'org-page)
(setq op/repository-directory "~/org/")
(setq op/site-domain "http://ridouchire.github.io")
